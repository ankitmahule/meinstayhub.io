# Fresh Movies Trailers
This is a simple website that provides a list of latest and popular movies and by clicking on them you can view their trailers.

## Installation
1. First of all you will need the following softwares installed on you machine
    - **Python 2.7**
    - **IDLE**, a software to compile and run the code
    - **Web browser**
2. Some Linux based Operating Systems comes built in with pyhton like Ubuntu, Fedora etc. but they don't have IDLE installed. On other Operating Systems the python and IDLE can be installed by downloading their setups respectively.
3. To install IDLE in Linux based OS type the command
    `$ sudo apt-get install idle`
4. Now check whether these all python files are available in project folder or not
    - **entertainment_center.py**
    - **media.py**
    - **fresh_tomatoes.py**
5. When these are files are available in the folder open the **entertainment_center.py** file in IDLE by going to menu **File -> Open Module -> Search the file in folder and press OK**.
6. Now as the file gets open you will run the file using either pressing **F5** or going to menu **Run -> Run Module**.
7. Now, the file will compile and execute the code and will open the browser with a page called **fresh_tomatoes.html** which contains the website. There are various kinds of movies displayed row by row. You can click on them and view the trailer of the movie.


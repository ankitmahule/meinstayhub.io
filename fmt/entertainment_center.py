import fresh_tomatoes
import media

#movies instances
cars = media.Movie("Cars",
                   "Blindsided by a new generation of blazing-fast cars, the legendary Lighting McQueen finds himself pushed out of the sport that he loves.",
                   "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=http%3A%2F%2Faolx.tmsimg.com%2Fmovieposters%2Fv7%2FAllPhotos%2F12216968%2Fp12216968_p_v7_ac.jpg%3Fw%3D270&client=cbc79c14efcebee57402&signature=d1d669fff37f96462e63b32b45b7aef191f18789",
                   "https://www.youtube.com/watch?v=dN26X92SOu8")

toy_story = media.Movie("Toy Story",
                        "A story of a boy and his toys that come to life",
                        "http://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg",
                        "https://www.youtube.com/watch?v=KYz2wyBy3kc")

railroad_tigers = media.Movie("Rail Road Tigers",
                              "A railroad worker (Jackie Chan) and his ragtag group of freedom fighters find themselves on the wrong side of the tracks when they decide to ambush a heavily armed military train filled with desperately needed provisions.",
                              "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=http%3A%2F%2Faolx.tmsimg.com%2Fmovieposters%2Fv7%2FNowShowing%2F13516620%2Fp13516620_p_v7_ab.jpg%3Fw%3D270&client=cbc79c14efcebee57402&signature=5a60cabe12694af9ee8a21becaf4e8df67c68e1a",
                              "https://www.youtube.com/watch?v=xzyE4m1BDkI")

max_steel = media.Movie("Max Steel",
                        "Teenager Max McGrath (Ben Winchell) discovers that his body can generate the most powerful energy in the universe. Steel (Josh Brener) is a funny, slightly rebellious, techno-organic extraterrestrial who wants to utilize Max's skills.",
                        "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=https%3A%2F%2Fs3.amazonaws.com%2Fmoviefone%2Fimages%2Fposters%2F20068147_poster_148037253453.jpg&client=cbc79c14efcebee57402&signature=ac9c1d026f7a051132f07362911de6f6ec587dfb",
                        "https://www.youtube.com/watch?v=QbJM3FF3Lbc")

altitude = media.Movie("Altitude",
                       "Gretchen Blair is a headstrong FBI agent who goes rogue during a hostage negotiation and is sent packing to a desk job back in Washington, D.C. As soon as her flight takes off, her seatmate offers her millions of dollars if she can get him off the plane alive.",
                       "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=http%3A%2F%2Faolx.tmsimg.com%2Fmovieposters%2Fv7%2FAllPhotos%2F13888905%2Fp13888905_p_v7_aa.jpg%3Fw%3D270&client=cbc79c14efcebee57402&signature=f34cc2a9ecc44964959f0ad4cee167ff7c311887",
                       "https://www.youtube.com/watch?v=JMdkofVdSIY")

el_americano = media.Movie("El Americano",
                        "When a gang of bully birds threaten his father and take over their circus, Cuco the parrot heads to Hollywood to ask TV superhero El Americano for help.",
                        "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=http%3A%2F%2Faolx.tmsimg.com%2Fmovieposters%2Fv7%2FAllPhotos%2F11588283%2Fp11588283_p_v7_ad.jpg%3Fw%3D270&client=cbc79c14efcebee57402&signature=4dd015bff7f7e561c2606283c9e553bd481e1d1c",
                        "https://www.youtube.com/watch?v=ap_53-uJKsw")

transformers = media.Movie("Transformers-The Last Knight",
                           "Humans are at war with the Transformers, and Optimus Prime is gone. The key to saving the future lies buried in the secrets of the past and the hidden history of Transformers on Earth.",
                           "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=https%3A%2F%2Fs3.amazonaws.com%2Fmoviefone%2Fimages%2Fposters%2Ftransformers-the-last-knight-poster_1486147874.jpg&client=cbc79c14efcebee57402&signature=a02d5d29c63018e3d71d21cc0713f8040a222b5f",
                           "https://www.youtube.com/watch?v=yCOvcyfRPRk")

wonder_woman = media.Movie("Wonder Woman",
                           "Before she was Wonder Woman (Gal Gadot), she was Diana, princess of the Amazons, trained to be an unconquerable warrior.",
                           "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=https%3A%2F%2Fs3.amazonaws.com%2Fmoviefone%2Fimages%2Fposters%2Fwonder-woman-kneel-poster_1489524873.jpg&client=cbc79c14efcebee57402&signature=795c30f30434730b21074f6d53b05236a8bed51b",
                           "https://www.youtube.com/watch?v=VSB4wGIdDwo")

mummy = media.Movie("The Mummy",
                    "Nick Morton is a soldier of fortune who plunders ancient sites for timeless artifacts and sells them to the highest bidder.",
                    "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=https%3A%2F%2Fs3.amazonaws.com%2Fmoviefone%2Fimages%2Fposters%2Fmmyadv1shttcruisergb3smrgb_1492617113.jpg&client=cbc79c14efcebee57402&signature=91b2d0f3cb525b87f45f1425983c1d8a0a3e36ca",
                    "https://www.youtube.com/watch?v=IjHgzkQM2Sg")

exception = media.Movie("The Exception",
                        "German soldier Stefan Brandt goes on a mission to investigate exiled German Monarch Kaiser Wilhelm II.",
                        "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=http%3A%2F%2Faolx.tmsimg.com%2Fmovieposters%2Fv7%2FNowShowing%2F13888273%2Fp13888273_p_v7_aa.jpg%3Fw%3D270&client=cbc79c14efcebee57402&signature=88ee0de7549c28300c4f2808debb60a530b6378a",
                        "https://www.youtube.com/watch?v=ld4eE2HU-ig")

mine = media.Movie("Mine",
                   "Trapped in the desert after standing on a land mine, a Marine sniper must single-handedly fight off the enemy while waiting more than two days for a convoy to rescue him.",
                   "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=https%3A%2F%2Fs3.amazonaws.com%2Fmoviefone%2Fimages%2Fposters%2Fmine2764x4096-poster_1487353313.jpg&client=cbc79c14efcebee57402&signature=c58162d8ab8ace5ad44cd5388b2b0ee3cd49829a",
                   "https://www.youtube.com/watch?v=UUKT-W0sr0M")

john_wick = media.Movie("John Wick 2",
                        "Retired super-assassin John Wick's plans to resume a quiet civilian life are cut short when Italian gangster Santino D'Antonio shows up on his doorstep with a gold marker, compelling him to repay past favors.",
                        "https://o.aolcdn.com/images/dims?resize=270%2C400&quality=70&image_uri=https%3A%2F%2Fs3.amazonaws.com%2Fmoviefone%2Fimages%2Fposters%2F20079352_poster_148037261514.jpg&client=cbc79c14efcebee57402&signature=cfc74e67065235be005cb27896c3656eb213054c",
                        "https://www.youtube.com/watch?v=XGk2EfbD_Ps")

#list containing movie instances
movies = [toy_story,cars,railroad_tigers,max_steel,altitude,el_americano,transformers,wonder_woman,mummy,exception,mine,john_wick]

#open the web browser using open_movies_page showing movies list
fresh_tomatoes.open_movies_page(movies)

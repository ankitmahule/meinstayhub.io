# Portfolio Project
This is a simple website that displays the portfolio and contains information such as About, Educational Background, Professional Experience, Hobbies and Interests etc. It also features the works done and links to see those works.

## Installation
1. First of all you will need the following softwares installed on you machine
    - **Web browser (Mozilla (any version),Chrome (any version),Internet Explorer 9+)**
2. Now as the browser is opened, you have to open the index.html in the project folder. Then you will be able to see the website.

